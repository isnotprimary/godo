package godo

import "sort"

// SortTasks provides a default sorting for a slice of tasks. Tasks with a priority will be at the start of the slice
// beginning with "A"s. Matching priority will sort on position. All tasks without a priority will follow, sorted
// by position.
func SortTasks(tasks []*Task) []*Task {
	sort.Slice(tasks, func(i, j int) bool {
		t1 := tasks[i]
		t2 := tasks[j]
		switch {
		case t1.Priority != "" && t2.Priority == "":
			return true
		case t1.Priority == "" && t2.Priority != "":
			return false
		case t1.Priority != "" && t2.Priority != "":
			if t1.Priority == t2.Priority {
				return t1.Position() < t2.Position()
			}
			return t1.Priority[0] < t2.Priority[0]
		default:
			return t1.Position() < t2.Position()
		}
	})
	return tasks
}
