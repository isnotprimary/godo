package godo

import (
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/muesli/termenv"
)

const (
	// DateFormat The format used for dates on disk
	DateFormat = "2006-01-02"
)

var priorityRegex = regexp.MustCompile(`\([a-zA-Z]\)`)

// Task represents a single entry of a TODOTXT. The description part is not directly exposed.
// Use UpdateDescription and Description instead.
type Task struct {
	// The completion status of a Task. True if the task is marked as complete.
	Complete bool
	// The priority of the task. A to Z uppercase. Optional
	Priority string
	// The date the task was completed on. Format of DateFormat. Optional
	CompletionDate string
	// The date the task was created on. Format of DateFormat. Optional unless completion date is set
	CreationDate string

	position    int
	description []string
	projectTag  string
	contextTag  string
	specials    map[string]string
}

// GetTagValue returns a value of a tag and true if the tag is present in the description of a Task or an empty string
// false
func (t *Task) GetTagValue(tag string) (string, bool) {
	val, ok := t.specials[tag]
	return val, ok
}

// UpdateTag updates the value of a tag in the description. Returns true if it was able to update the tag. False if not
// e.g. it was missing
func (t *Task) UpdateTag(key, newValue string) bool {
	oldValue, ok := t.GetTagValue(key)
	if !ok {
		return false
	}

	oldTag := fmt.Sprintf("%s:%s", key, oldValue)
	newTag := fmt.Sprintf("%s:%s", key, newValue)
	for i, part := range t.description {
		if part == oldTag {
			t.description[i] = newTag
		}
	}
	t.specials[key] = newValue

	return true
}

// String render the Task as a string as it would be represented in the TODODir
func (t *Task) String() string {
	var parts []string
	if t.Complete {
		parts = append(parts, "x")
	}
	if t.Priority != "" {
		parts = append(parts, fmt.Sprintf("(%s)", t.Priority))
	}
	if t.CompletionDate != "" {
		parts = append(parts, t.CompletionDate)
	}
	if t.CreationDate != "" {
		parts = append(parts, t.CreationDate)
	}
	parts = append(parts, t.description...)

	return strings.Join(parts, " ")
}

// Style provides a termenv.Style that would be used to render this task
func (t *Task) Style() termenv.Style {
	return t.colorString("")
}

// Render provides a default rendering including task number if showPos is true and colors if color is true
func (t *Task) Render(showPos, color bool) string {
	var raw string
	if t.position > 0 && showPos {
		raw = fmt.Sprintf("%d %s", t.position, t.String())
	} else {
		raw = t.String()
	}
	if color {
		raw = t.colorString(raw).String()
	}
	return raw
}

func (t *Task) colorString(raw string) termenv.Style {
	s := termenv.String(raw)
	p := termenv.ColorProfile()

	// Always apply a minimum color set
	if p == termenv.Ascii {
		p = termenv.ANSI
	}

	var forground termenv.Color
	switch {
	case t.Complete:
		forground = p.Color("7")
	case t.Priority != "":
		colors := []string{"11", "2", "4", "15"}
		pos := (int(t.Priority[0]) - 65) % len(colors)
		forground = p.Color(colors[pos])
	}
	s = s.Foreground(forground)
	return s
}

// Parse marshals a TODOTXT string into a struct
func Parse(raw string) (*Task, error) {
	t := &Task{}
	parts := strings.Split(raw, " ")
	for i, part := range parts {
		switch {
		case isCompletion(i, part):
			t.Complete = true
		case isPriority(i, part):
			t.Priority = strings.ToUpper(string(part[1]))
		case isDate(i, part) && t.Complete && t.CompletionDate == "":
			t.CompletionDate = part
		case isDate(i, part) && t.CreationDate == "":
			t.CreationDate = parts[i]
			t.resetDescription(parts[i+1:])
			return t, nil
		default:
			t.resetDescription(parts[i:])
			return t, nil
		}
	}

	return t, nil
}

// UpdateDescription allows changes the description part of a Task
func (t *Task) UpdateDescription(description string) {
	parts := strings.Split(description, " ")
	t.resetDescription(parts)
}

// Description retrieves the currently set description as a string
func (t *Task) Description() string {
	return strings.Join(t.description, " ")
}

// Position retrieves the current position of the task in the todotxt
func (t *Task) Position() int {
	return t.position
}

// MarkDone sets Complete to true and sets the CompletionDate to today's date, if it is not already set
func (t *Task) MarkDone() {
	t.Complete = true
	if t.CompletionDate == "" {
		t.CompletionDate = time.Now().Format(DateFormat)
	}
}

func (t *Task) resetDescription(parts []string) {
	t.specials = nil
	t.contextTag = ""
	t.projectTag = ""
	for _, part := range parts {
		if isProjectTag(part) {
			t.projectTag = part
		} else if isContextTag(part) {
			t.contextTag = part
		} else if isKeyValue(part) {
			keyVal := strings.Split(part, ":")
			if t.specials == nil {
				t.specials = map[string]string{}
			}
			t.specials[keyVal[0]] = keyVal[1]
		}
	}
	t.description = parts
}

func isCompletion(pos int, part string) bool {
	return pos == 0 && part == "x"
}

func isPriority(pos int, part string) bool {
	return pos <= 1 && priorityRegex.MatchString(part)
}

func isDate(pos int, part string) bool {
	if pos >= 4 {
		return false
	}
	_, err := time.Parse(DateFormat, part)
	return err == nil
}

func isProjectTag(part string) bool {
	return part[0] == '+'
}

func isContextTag(part string) bool {
	return part[0] == '@'
}

func isKeyValue(part string) bool {
	return len(strings.Split(part, ":")) == 2
}
