package godo

import (
	"bufio"
	"fmt"
	"os"
	"testing"
	"time"

	gocmp "github.com/google/go-cmp/cmp"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"
)

func TestConfig_AddTask(t *testing.T) {
	today := time.Now().Format(DateFormat)
	tests := []struct {
		name          string
		createFile    bool
		startContents []string
		add           []string
		result        []string
		pos           []int
		addDate       bool
	}{
		{
			name:       "No file exists",
			createFile: false,
			add:        []string{"(A) here is a nice task due:2022-05-1"},
			result:     []string{"(A) here is a nice task due:2022-05-1"},
			pos:        []int{1},
		},
		{
			name:       "Empty file exists",
			createFile: true,
			add:        []string{"(A) here is a nice task due:2022-05-1"},
			result:     []string{"(A) here is a nice task due:2022-05-1"},
			pos:        []int{1},
		},
		{
			name:          "File with new line ending exists",
			createFile:    true,
			startContents: []string{"(A) task 1 due:2022-06-1\n"},
			add:           []string{"(A) task 2 due:2022-05-1"},
			result:        []string{"(A) task 1 due:2022-06-1", "(A) task 2 due:2022-05-1"},
			pos:           []int{2},
		},
		{
			name:          "File without new line ending exists",
			createFile:    true,
			startContents: []string{"(A) task 1 due:2022-06-1"},
			add:           []string{"(A) task 2 due:2022-05-1"},
			result:        []string{"(A) task 1 due:2022-06-1", "(A) task 2 due:2022-05-1"},
			pos:           []int{2},
		},
		{
			name:       "Add multiple lines",
			createFile: false,
			add: []string{
				"(A) here is a nice task due:2022-05-1",
				"(A) task 1 due:2022-06-1",
				"(A) task 2 due:2022-05-1"},
			result: []string{
				"(A) here is a nice task due:2022-05-1",
				"(A) task 1 due:2022-06-1",
				"(A) task 2 due:2022-05-1"},
			pos: []int{1, 2, 3},
		},
		{
			name:       "Add date",
			createFile: false,
			add: []string{
				"(A) here is a nice task due:2022-05-1",
				"(A) 2022-05-01 task 1 due:2022-06-1",
				"(A) task 2 due:2022-05-1"},
			result: []string{
				fmt.Sprintf("(A) %s here is a nice task due:2022-05-1", today),
				"(A) 2022-05-01 task 1 due:2022-06-1",
				fmt.Sprintf("(A) %s task 2 due:2022-05-1", today)},
			pos:     []int{1, 2, 3},
			addDate: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir, err := os.MkdirTemp("", "godo")
			assert.NilError(t, err)
			defer os.RemoveAll(dir)

			if tt.createFile {
				testFile, err := os.OpenFile(dir+"/todo.txt", os.O_RDWR|os.O_CREATE, 0600)
				assert.NilError(t, err)
				for _, startingTask := range tt.startContents {
					_, err = testFile.WriteString(startingTask)
					assert.NilError(t, err)
				}
			}

			c := &Config{
				Env: Env{
					TODODir:   dir,
					DateOnAdd: tt.addDate,
				},
			}

			for i, toAdd := range tt.add {
				task, err := Parse(toAdd)
				assert.NilError(t, err)
				err = c.AddTask(task)
				assert.NilError(t, err)
				assert.Check(t, cmp.Equal(task.Position(), tt.pos[i]))
			}

			compareFile(t, c, false, tt.result)
		})
	}
}

func TestConfig_Tasks(t *testing.T) {
	tests := []struct {
		name  string
		start string
		want  []*Task
	}{
		{
			name:  "A single simple task",
			start: "A simple task",
			want: []*Task{{
				position:    1,
				description: []string{"A", "simple", "task"},
			}},
		},
		{
			name:  "2 simple tasks",
			start: "A simple task\nA simple task",
			want: []*Task{
				{
					position:    1,
					description: []string{"A", "simple", "task"},
				},
				{
					position:    2,
					description: []string{"A", "simple", "task"},
				},
			},
		},
		{
			name:  "2 tasks with preserved lines",
			start: "A simple task\n\nA simple task",
			want: []*Task{
				{
					position:    1,
					description: []string{"A", "simple", "task"},
				},
				{
					position:    3,
					description: []string{"A", "simple", "task"},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir, err := os.MkdirTemp("", "godo")
			assert.NilError(t, err)
			defer os.RemoveAll(dir)

			err = os.WriteFile(dir+"/todo.txt", []byte(tt.start), 0600)
			assert.NilError(t, err)

			c := &Config{
				Env: Env{
					TODODir: dir,
				},
			}

			got, err := c.Tasks()
			assert.NilError(t, err)
			assert.Check(t, cmp.DeepEqual(got, tt.want, gocmp.AllowUnexported(Task{})))
		})
	}
}

func TestConfig_DoneTasks(t *testing.T) {
	tests := []struct {
		name  string
		start string
		want  []*Task
	}{
		{
			name:  "A single simple task",
			start: "x A simple task",
			want: []*Task{{
				position:    1,
				description: []string{"A", "simple", "task"},
				Complete:    true,
			}},
		},
		{
			name:  "2 simple tasks",
			start: "x A simple task\nx A simple task",
			want: []*Task{
				{
					position:    1,
					description: []string{"A", "simple", "task"},
					Complete:    true,
				},
				{
					position:    2,
					description: []string{"A", "simple", "task"},
					Complete:    true,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir, err := os.MkdirTemp("", "godo")
			assert.NilError(t, err)
			defer os.RemoveAll(dir)

			err = os.WriteFile(dir+"/done.txt", []byte(tt.start), 0600)
			assert.NilError(t, err)

			c := &Config{
				Env: Env{
					TODODir: dir,
				},
			}

			got, err := c.DoneTasks()
			assert.NilError(t, err)
			assert.Check(t, cmp.DeepEqual(got, tt.want, gocmp.AllowUnexported(Task{})))
		})
	}
}

func TestConfig_UpdateTask(t *testing.T) {

	tests := []struct {
		name          string
		startContents []string
		update        *Task
		result        []string
		wantErr       string
	}{
		{
			name:          "File with new line ending exists",
			startContents: []string{"(A) task 1 due:2022-06-1\n"},
			update: &Task{
				Priority:    "B",
				position:    1,
				description: []string{"prior", "b", "now", "due:2022-05-1"},
				specials:    map[string]string{"due": "2022-05-1"},
			},
			result: []string{"(B) prior b now due:2022-05-1"},
		},
		{
			name: "File with multiple lines",
			startContents: []string{
				"(A) task 1 due:2022-06-1\n",
				"(B) task 2 due:2022-06-2\n",
				"(C) task 3 due:2022-06-3\n",
			},
			update: &Task{
				Priority:    "B",
				position:    2,
				description: []string{"due", "earlier", "now", "due:2022-05-1"},
				specials:    map[string]string{"due": "2022-05-1"},
			},
			result: []string{
				"(A) task 1 due:2022-06-1",
				"(B) due earlier now due:2022-05-1",
				"(C) task 3 due:2022-06-3",
			},
		},
		{
			name: "File with multiple lines and a preserved line",
			startContents: []string{
				"(A) task 1 due:2022-06-1\n",
				"\n",
				"(B) task 2 due:2022-06-2\n",
				"(C) task 3 due:2022-06-3\n",
			},
			update: &Task{
				Priority:    "B",
				position:    3,
				description: []string{"due", "earlier", "now", "due:2022-05-1"},
				specials:    map[string]string{"due": "2022-05-1"},
			},
			result: []string{
				"(A) task 1 due:2022-06-1",
				"",
				"(B) due earlier now due:2022-05-1",
				"(C) task 3 due:2022-06-3",
			},
		},
		{
			name:          "Task does not exist",
			startContents: []string{"(A) task 1 due:2022-06-1\n"},
			update: &Task{
				Priority:    "B",
				position:    2,
				description: []string{"prior", "b", "now", "due:2022-05-1"},
				specials:    map[string]string{"due": "2022-05-1"},
			},
			result:  []string{"(A) task 1 due:2022-06-1"},
			wantErr: "task(s) [2] not found",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir, err := os.MkdirTemp("", "godo")
			assert.NilError(t, err)
			defer os.RemoveAll(dir)

			populateFile(t, dir, false, tt.startContents)

			c := &Config{
				Env: Env{
					TODODir: dir,
				},
			}

			err = c.UpdateTask(tt.update)
			if tt.wantErr != "" {
				assert.Check(t, cmp.ErrorContains(err, tt.wantErr))
			} else {
				assert.NilError(t, err)
			}

			compareFile(t, c, false, tt.result)
		})
	}
}

func TestConfig_UpdateTasks(t *testing.T) {

	tests := []struct {
		name          string
		startContents []string
		update        []*Task
		result        []string
		wantErr       string
	}{
		{
			name:          "File with new line ending exists",
			startContents: []string{"(A) task 1 due:2022-06-1\n"},
			update: []*Task{
				{
					Priority:    "B",
					position:    1,
					description: []string{"prior", "b", "now", "due:2022-05-1"},
					specials:    map[string]string{"due": "2022-05-1"},
				},
			},
			result: []string{"(B) prior b now due:2022-05-1"},
		},
		{
			name: "File with multiple lines",
			startContents: []string{
				"(A) task 1 due:2022-06-1\n",
				"(B) task 2 due:2022-06-2\n",
				"(C) task 3 due:2022-06-3\n",
			},
			update: []*Task{
				{
					Priority:    "B",
					position:    2,
					description: []string{"due", "earlier", "now", "due:2022-05-1"},
					specials:    map[string]string{"due": "2022-05-1"},
				},
			},
			result: []string{
				"(A) task 1 due:2022-06-1",
				"(B) due earlier now due:2022-05-1",
				"(C) task 3 due:2022-06-3",
			},
		},
		{
			name: "File with multiple lines and multiple updates",
			startContents: []string{
				"(A) task 1 due:2022-06-1\n",
				"(B) task 2 due:2022-06-2\n",
				"(C) task 3 due:2022-06-3\n",
				"(D) task 4 due:2022-06-4\n",
			},
			update: []*Task{
				{
					Priority:    "B",
					position:    2,
					description: []string{"due", "earlier", "now", "due:2022-05-1"},
					specials:    map[string]string{"due": "2022-05-1"},
				},
				{
					Complete:    true,
					Priority:    "D",
					position:    4,
					description: []string{"task", "d", "due:2022-06-4"},
					specials:    map[string]string{"due": "2022-06-4"},
				},
			},
			result: []string{
				"(A) task 1 due:2022-06-1",
				"(B) due earlier now due:2022-05-1",
				"(C) task 3 due:2022-06-3",
				"x (D) task d due:2022-06-4",
			},
		},
		{
			name:          "Task does not exist",
			startContents: []string{"(A) task 1 due:2022-06-1\n"},
			update: []*Task{
				{
					Priority:    "B",
					position:    2,
					description: []string{"prior", "b", "now", "due:2022-05-1"},
					specials:    map[string]string{"due": "2022-05-1"},
				},
				{
					Priority:    "B",
					position:    7,
					description: []string{"prior", "b", "now", "due:2022-05-1"},
					specials:    map[string]string{"due": "2022-05-1"},
				},
			},
			result:  []string{"(A) task 1 due:2022-06-1"},
			wantErr: "task(s) [2 7] not found",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir, err := os.MkdirTemp("", "godo")
			assert.NilError(t, err)
			defer os.RemoveAll(dir)

			populateFile(t, dir, false, tt.startContents)

			c := &Config{
				Env: Env{
					TODODir: dir,
				},
			}

			err = c.UpdateTasks(tt.update)
			if tt.wantErr != "" {
				assert.Check(t, cmp.ErrorContains(err, tt.wantErr))
			} else {
				assert.NilError(t, err)
			}

			compareFile(t, c, false, tt.result)
		})
	}
}

func TestConfig_DeleteTasks(t *testing.T) {
	tests := []struct {
		name          string
		startContents []string
		delete        []*Task
		donetxt       bool
		preserveLines bool
		result        []string
		wantErr       string
	}{
		{
			name:          "File with new line ending exists",
			startContents: []string{"(A) task 1 due:2022-06-1\n"},
			delete: []*Task{
				{
					position: 1,
				},
			},
			result: []string{},
		},
		{
			name: "File with multiple lines",
			startContents: []string{
				"(A) task 1 due:2022-06-1\n",
				"(B) task 2 due:2022-06-2\n",
				"(C) task 3 due:2022-06-3\n",
			},
			delete: []*Task{
				{
					position: 2,
				},
			},
			result: []string{
				"(A) task 1 due:2022-06-1",
				"(C) task 3 due:2022-06-3",
			},
		},
		{
			name: "File with multiple lines and multiple deletes",
			startContents: []string{
				"(A) task 1 due:2022-06-1\n",
				"(B) task 2 due:2022-06-2\n",
				"(C) task 3 due:2022-06-3\n",
				"(D) task 4 due:2022-06-4\n",
			},
			delete: []*Task{
				{
					position: 2,
				},
				{
					position: 4,
				},
			},
			result: []string{
				"(A) task 1 due:2022-06-1",
				"(C) task 3 due:2022-06-3",
			},
		},
		{
			name:    "donetxt",
			donetxt: true,
			startContents: []string{
				"(A) task 1 due:2022-06-1\n",
				"(B) task 2 due:2022-06-2\n",
				"(C) task 3 due:2022-06-3\n",
				"(D) task 4 due:2022-06-4\n",
			},
			delete: []*Task{
				{
					position: 2,
				},
				{
					position: 4,
				},
			},
			result: []string{
				"(A) task 1 due:2022-06-1",
				"(C) task 3 due:2022-06-3",
			},
		},
		{
			name:          "preserve line numbers",
			preserveLines: true,
			startContents: []string{
				"(A) task 1 due:2022-06-1\n",
				"\n",
				"(B) task 2 due:2022-06-2\n",
				"\n",
				"(C) task 3 due:2022-06-3\n",
			},
			delete: []*Task{
				{
					position: 3,
				},
			},
			result: []string{
				"(A) task 1 due:2022-06-1",
				"",
				"",
				"(C) task 3 due:2022-06-3",
			},
		},
		{
			name:          "Task does not exist",
			startContents: []string{"(A) task 1 due:2022-06-1\n"},
			delete: []*Task{
				{
					position: 2,
				},
				{
					position: 7,
				},
			},
			result:  []string{"(A) task 1 due:2022-06-1"},
			wantErr: "task(s) [2 7] not found",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir, err := os.MkdirTemp("", "godo")
			assert.NilError(t, err)
			defer os.RemoveAll(dir)

			populateFile(t, dir, tt.donetxt, tt.startContents)

			c := &Config{
				Env: Env{
					TODODir: dir,
				},
			}

			err = c.DeleteTasks(tt.delete, tt.donetxt)
			if tt.wantErr != "" {
				assert.Check(t, cmp.ErrorContains(err, tt.wantErr))
			} else {
				assert.NilError(t, err)
			}

			compareFile(t, c, tt.donetxt, tt.result)
		})
	}
}

func TestConfig_DoTasks(t *testing.T) {
	today := time.Now().Format(DateFormat)
	tests := []struct {
		name       string
		todoStart  []string
		doneStart  []string
		tasks      []*Task
		archive    bool
		todoResult []string
		doneResult []string
		wantErr    string
	}{
		{
			name:      "1 task no archive",
			todoStart: []string{"A task"},
			tasks: []*Task{
				{
					position:    1,
					description: []string{"A", "task"},
				},
			},
			archive:    false,
			todoResult: []string{fmt.Sprintf("x %s A task", today)},
		},
		{
			name:      "2 task no archive",
			todoStart: []string{"A task\n", "Another\n", "(B) third"},
			tasks: []*Task{
				{
					position:    1,
					description: []string{"A", "task"},
				},
				{
					position:    3,
					Priority:    "B",
					description: []string{"third"},
				},
			},
			archive: false,
			todoResult: []string{
				fmt.Sprintf("x %s A task", today),
				"Another",
				fmt.Sprintf("x (B) %s third", today),
			},
		},
		{
			name:      "1 task archive",
			todoStart: []string{"A task\n", "task 2"},
			tasks: []*Task{
				{
					position:    1,
					description: []string{"A", "task"},
				},
			},
			archive:    true,
			todoResult: []string{"task 2"},
			doneResult: []string{fmt.Sprintf("x %s A task", today)},
		},
		{
			name:      "2 task archive",
			todoStart: []string{"A task\n", "Another\n", "(B) third"},
			tasks: []*Task{
				{
					position:    1,
					description: []string{"A", "task"},
				},
				{
					position:    3,
					Priority:    "B",
					description: []string{"third"},
				},
			},
			archive:    true,
			todoResult: []string{"Another"},
			doneResult: []string{fmt.Sprintf("x %s A task", today), fmt.Sprintf("x (B) %s third", today)},
		},
		{
			name:      "unknown task",
			todoStart: []string{"A task\n", "task 2"},
			tasks: []*Task{
				{
					position:    3,
					description: []string{"A", "task"},
				},
			},
			archive:    false,
			todoResult: []string{"A task", "task 2"},
			wantErr:    "task(s) [3] not found",
		},
		{
			name:      "unknown task with archive",
			todoStart: []string{"A task\n", "task 2"},
			tasks: []*Task{
				{
					position:    3,
					description: []string{"A", "task"},
				},
			},
			archive:    true,
			todoResult: []string{"A task", "task 2"},
			wantErr:    "task(s) [3] not found",
		},
		{
			name:      "mutated task gets update",
			todoStart: []string{"A task\n", "task 2"},
			tasks: []*Task{
				{
					position:    1,
					description: []string{"B", "task"},
				},
			},
			archive:    true,
			todoResult: []string{"task 2"},
			doneResult: []string{fmt.Sprintf("x %s B task", today)},
		},
		{
			name:      "Specified completion does not get updated",
			todoStart: []string{"A task"},
			tasks: []*Task{
				{
					CompletionDate: "1950-01-01",
					position:       1,
					description:    []string{"A", "task"},
				},
			},
			archive:    false,
			todoResult: []string{"x 1950-01-01 A task"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dir, err := os.MkdirTemp("", "godo")
			assert.NilError(t, err)
			defer os.RemoveAll(dir)

			if len(tt.todoStart) > 0 {
				populateFile(t, dir, false, tt.todoStart)
			}
			if len(tt.doneStart) > 0 {
				populateFile(t, dir, true, tt.doneStart)
			}

			c := &Config{
				Env: Env{
					TODODir: dir,
				},
			}

			err = c.DoTasks(tt.tasks, tt.archive)
			if tt.wantErr != "" {
				assert.Check(t, cmp.ErrorContains(err, tt.wantErr))
			} else {
				assert.NilError(t, err)
			}

			compareFile(t, c, false, tt.todoResult)
			compareFile(t, c, true, tt.doneResult)
		})
	}
}

func populateFile(t *testing.T, dir string, archiveFile bool, contents []string) {
	fileName := "todo.txt"
	if archiveFile {
		fileName = "done.txt"
	}
	testFile, err := os.OpenFile(dir+"/"+fileName, os.O_RDWR|os.O_CREATE, 0600)
	assert.NilError(t, err)
	defer testFile.Close()
	for _, startingTask := range contents {
		_, err = testFile.WriteString(startingTask)
		assert.NilError(t, err)
	}
}

func compareFile(t *testing.T, c *Config, archiveFile bool, contents []string) {
	var file *os.File
	var err error
	if archiveFile {
		file, err = c.Donetxt()
	} else {
		file, err = c.Todotxt()
	}
	assert.NilError(t, err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	i := 0
	for scanner.Scan() {
		assert.Assert(t, i <= len(contents), "file contains unexpected content")
		assert.Check(t, cmp.Equal(scanner.Text(), contents[i]))
		i++
	}
	assert.NilError(t, scanner.Err())
	assert.Check(t, cmp.Equal(i, len(contents)))
}
