package godo

import (
	"testing"
	"time"

	gocmp "github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"
)

func TestTask_Parse(t *testing.T) {
	tests := []struct {
		name      string
		raw       string
		stringOut string
		want      *Task
		wantErr   bool
	}{
		{
			name: "Just a description",
			raw:  "Just this",
			want: &Task{
				description: []string{"Just", "this"},
			},
			wantErr: false,
		},
		{
			name: "Just completed",
			raw:  "x I am done",
			want: &Task{
				Complete:    true,
				description: []string{"I", "am", "done"},
			},
			wantErr: false,
		},
		{
			name: "Just priority",
			raw:  "(A) I need to be done",
			want: &Task{
				Priority:    string('A'),
				description: []string{"I", "need", "to", "be", "done"},
			},
			wantErr: false,
		},
		{
			name:      "Lower priority",
			raw:       "(b) I need to be done",
			stringOut: "(B) I need to be done",
			want: &Task{
				Priority:    string('B'),
				description: []string{"I", "need", "to", "be", "done"},
			},
			wantErr: false,
		},
		{
			name: "Just creation date",
			raw:  "2022-07-07 I am to be done",
			want: &Task{
				CreationDate: "2022-07-07",
				description:  []string{"I", "am", "to", "be", "done"},
			},
			wantErr: false,
		},
		{
			name: "No creation date on complete",
			raw:  "x 2022-07-07 I was made, I was done",
			want: &Task{
				CompletionDate: "2022-07-07",
				Complete:       true,
				description:    []string{"I", "was", "made,", "I", "was", "done"},
			},
			wantErr: false,
		},
		{
			name: "Creation and complete date",
			raw:  "x 2022-07-07 2022-07-06 I was made, I was done",
			want: &Task{
				CompletionDate: "2022-07-07",
				CreationDate:   "2022-07-06",
				Complete:       true,
				description:    []string{"I", "was", "made,", "I", "was", "done"},
			},
			wantErr: false,
		},
		{
			name: "Creation and complete date and priority",
			raw:  "x (A) 2022-07-07 2022-07-06 I was made, I was done",
			want: &Task{
				CompletionDate: "2022-07-07",
				CreationDate:   "2022-07-06",
				Priority:       "A",
				Complete:       true,
				description:    []string{"I", "was", "made,", "I", "was", "done"},
			},
			wantErr: false,
		},
		{
			name: "Creation and completion date on incomplete",
			raw:  "2022-07-08 2022-07-07 I was made, I was done",
			want: &Task{
				CreationDate: "2022-07-08",
				Complete:     false,
				description:  []string{"2022-07-07", "I", "was", "made,", "I", "was", "done"},
			},
			wantErr: false,
		},
		{
			name: "Project Tag",
			raw:  "I have +project",
			want: &Task{
				description: []string{"I", "have", "+project"},
				projectTag:  "+project",
			},
			wantErr: false,
		},
		{
			name: "Context Tag",
			raw:  "I have @context",
			want: &Task{
				description: []string{"I", "have", "@context"},
				contextTag:  "@context",
			},
			wantErr: false,
		},
		{
			name: "Project and Context Tag",
			raw:  "I have @context & +project",
			want: &Task{
				description: []string{"I", "have", "@context", "&", "+project"},
				contextTag:  "@context",
				projectTag:  "+project",
			},
			wantErr: false,
		},
		{
			name: "Project and Context Tag in random order",
			raw:  "@context in the +project",
			want: &Task{
				description: []string{"@context", "in", "the", "+project"},
				contextTag:  "@context",
				projectTag:  "+project",
			},
			wantErr: false,
		},
		{
			name: "Due date",
			raw:  "I am due on due:2022-06-10",
			want: &Task{
				description: []string{"I", "am", "due", "on", "due:2022-06-10"},
				specials: map[string]string{
					"due": "2022-06-10",
				},
			},
			wantErr: false,
		},
		{
			name: "Any old tag",
			raw:  "any old:tag",
			want: &Task{
				description: []string{"any", "old:tag"},
				specials: map[string]string{
					"old": "tag",
				},
			},
			wantErr: false,
		},
		{
			name: "Not a priority",
			raw:  "2022-06-10 (A) thing",
			want: &Task{
				description:  []string{"(A)", "thing"},
				CreationDate: "2022-06-10",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Parse(tt.raw)
			if tt.wantErr {
				assert.Assert(t, err != nil)
				return
			}
			assert.NilError(t, err)
			assert.Check(t, cmp.DeepEqual(got, tt.want, gocmp.AllowUnexported(Task{})))

			t.Run("Check the output string", func(t *testing.T) {
				if tt.stringOut != "" {
					assert.Check(t, cmp.Equal(tt.stringOut, got.String()))
				} else {
					assert.Check(t, cmp.Equal(tt.raw, got.String()))
				}
			})
		})
	}
}

func TestTask_GetTags(t *testing.T) {
	tests := []struct {
		name        string
		description string
		tag         string
		value       string
		ok          bool
	}{
		{
			name:        "Tag found",
			description: "any old:tag",
			tag:         "old",
			value:       "tag",
			ok:          true,
		},
		{
			name:        "Correct of multiple tags found",
			description: "one:tag some text two:tags three:tagos",
			tag:         "two",
			value:       "tags",
			ok:          true,
		},
		{
			name:        "Tag not found if none exist",
			description: "any old tag",
			tag:         "old",
			value:       "tag",
			ok:          false,
		},
		{
			name:        "Tag not found if it doesn't exist",
			description: "any old:tag",
			tag:         "incorrect",
			value:       "tag",
			ok:          false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t1 *testing.T) {
			task := &Task{}
			task.UpdateDescription(tt.description)
			value, ok := task.GetTagValue(tt.tag)
			assert.Check(t, cmp.Equal(tt.ok, ok))
			if ok {
				assert.Check(t, cmp.Equal(tt.value, value))
			}
		})
	}
}

func TestTask_UpdateTags(t *testing.T) {
	tests := []struct {
		name           string
		description    string
		newDescription string
		tag            string
		value          string
		ok             bool
	}{
		{
			name:           "Tag found",
			description:    "any update:me",
			newDescription: "any update:no-you",
			tag:            "update",
			value:          "no-you",
			ok:             true,
		},
		{
			name:           "Tag not found",
			description:    "any update me",
			newDescription: "any update me",
			tag:            "update",
			value:          "no-you",
			ok:             false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t1 *testing.T) {
			task := &Task{}
			task.UpdateDescription(tt.description)
			ok := task.UpdateTag(tt.tag, tt.value)
			assert.Check(t, cmp.Equal(tt.ok, ok))
			if ok {
				assert.Check(t, cmp.Equal(tt.newDescription, task.Description()))
			}
		})
	}
}

func TestTask_Render(t *testing.T) {
	tests := []struct {
		name    string
		task    Task
		want    string
		showPos bool
		color   bool
	}{
		{
			name: "Just a description",
			task: Task{
				Complete:    false,
				position:    1,
				description: []string{"Just", "this"},
			},
			want: "Just this",
		},
		{
			name: "Just a A",
			task: Task{
				Complete:    false,
				position:    1,
				description: []string{"Just", "a", "A"},
				Priority:    "A",
			},
			want: "(A) Just a A",
		},
		{
			name: "A colorful A",
			task: Task{
				Complete:    false,
				position:    1,
				description: []string{"A", "colorful", "A"},
				Priority:    "A",
			},
			color: true,
			want:  "\u001B[93m(A) A colorful A\u001B[0m",
		},
		{
			name: "A colorful A with task id",
			task: Task{
				Complete:    false,
				position:    1,
				description: []string{"A", "colorful", "A"},
				Priority:    "A",
			},
			color:   true,
			showPos: true,
			want:    "\u001B[93m1 (A) A colorful A\u001B[0m",
		},
		{
			name: "A colorful B",
			task: Task{
				Complete:    false,
				position:    1,
				description: []string{"A", "colorful", "B"},
				Priority:    "B",
			},
			color: true,
			want:  "\u001B[32m(B) A colorful B\u001B[0m",
		},
		{
			name: "A colorful D",
			task: Task{
				Complete:    false,
				position:    1,
				description: []string{"A", "colorful", "D"},
				Priority:    "D",
			},
			color: true,
			want:  "\u001B[97m(D) A colorful D\u001B[0m",
		},
		{
			name: "A colorful E",
			task: Task{
				Complete:    false,
				position:    1,
				description: []string{"A", "colorful", "E"},
				Priority:    "E",
			},
			color: true,
			want:  "\u001B[93m(E) A colorful E\u001B[0m",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.task.Render(tt.showPos, tt.color)
			assert.Check(t, cmp.Equal(got, tt.want))
		})
	}
}

func TestTask_MarkDone(t *testing.T) {
	today := time.Now().Format(DateFormat)
	tests := []struct {
		name  string
		start *Task
		want  *Task
	}{
		{
			name: "complete no date",
			start: &Task{
				Complete: false,
			},
			want: &Task{
				Complete:       true,
				CompletionDate: today,
			},
		},
		{
			name: "complete dont override date",
			start: &Task{
				Complete:       false,
				CompletionDate: "1950-01-01",
			},
			want: &Task{
				Complete:       true,
				CompletionDate: "1950-01-01",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t1 *testing.T) {
			tt.start.MarkDone()
			assert.Check(t, cmp.DeepEqual(tt.start, tt.want, cmpopts.IgnoreUnexported(Task{})))
		})
	}
}
