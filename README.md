# GoDo
An entirely unnecessary go SDK for [todo.txt](https://github.com/todotxt/todo.txt-cli) add-ons.

## Motivation
Making add-ons for todo.txt is fairly easy. It has a fairly simple structured text format and only a few files and settings.
Whilst that means you don't really need an SDK for it, there is fun and good practice to be had in making one. 

## Getting started
TODO