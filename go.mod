module gitlab.com/isnotprimary/godo

go 1.18

require gotest.tools/v3 v3.3.0

require (
	github.com/alecthomas/kong v0.6.1
	github.com/google/go-cmp v0.5.8
	github.com/muesli/termenv v0.12.0
)

require (
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
)
