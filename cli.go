package godo

import (
	"os"

	"github.com/alecthomas/kong"
)

// Env is a kong compatible struct that can be included in a CLI configuration to account for the environment variables
// provided by todotxt. Use the CLI function to handle construction and usage message printing when the "usage"
// command is passed.
//
// Note: TODODir is set to "./testdata/" by default to aid in testing during development, but the todotxt CLI sets this
// variable
type Env struct {
	// PreserveLineNumbers leaves a blank line when a TODOTXT entry is deleted
	PreserveLineNumbers bool `env:"TODOTXT_PRESERVE_LINE_NUMBERS" hidden:"" default:"false"`
	// Verbose enables verbose log output
	Verbose bool `env:"TODOTXT_VERBOSE" hidden:"" default:"false"`
	// Plain disables color output
	Plain bool `env:"TODOTXT_PLAIN" hidden:"" default:"false"`
	// AutoArchive signifies completed todos should be moved to the archive file
	AutoArchive bool `env:"TODOTXT_AUTO_ARCHIVE" hidden:"" default:"false"`
	// Force disables confirmation messages
	Force bool `env:"TODOTXT_FORCE" hidden:"" default:"false"`
	// DateOnAdd means prepend the current date to a task when it's added
	DateOnAdd bool `env:"TODOTXT_DATE_ON_ADD" hidden:"" default:"false"`

	// Sh name of the todosh script, use in the usage message
	Sh string `env:"TODO_SH" hidden:""`
	// FullSh complete path to calling todosh script, use for invoking todosh
	FullSh string `env:"TODO_FULL_SH" hidden:""`
	// ConfigFile is the path to the users configuration file
	ConfigFile string `env:"TODOTXT_CFG_FILE" hidden:""`
	// TODODir is the directory containing the config file. Defaults to "./testdata/" for testing outside of the TODOTXT
	// runtime
	TODODir string `env:"TODO_DIR" hidden:"" default:"./testdata/" type:"existingdir"`
}

// CLI calls kong.Parse creating a *kong.Context. It handles the usage sub command, printing out the usage message and
// exiting if found. Example usage:
//  var CLI struct {
//	 godo.Env
//	 TaskNumbers bool `default:"true" help:"show tasks position in the file" short:"i"`
//  }
//
//  func main() {
//	 _ = godo.CLI(&CLI, "did", "See tasks in the archive file")
//	 cfg := godo.NewConfig(CLI.Env)
//   ...
//  }
func CLI(cli interface{}, name, description string, options ...kong.Option) *kong.Context {
	if os.Args[1] == "usage" {
		os.Args[1] = "-h"
	} else if len(os.Args) > 2 {
		os.Args = append(os.Args[:1], os.Args[2:]...)
	} else {
		os.Args = os.Args[:1]
	}
	options = append(options, kong.Name(name), kong.Description(description))
	return kong.Parse(cli, options...)
}
