package godo

import (
	"testing"

	gocmp "github.com/google/go-cmp/cmp"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"
)

func TestSortTasks(t *testing.T) {
	tests := []struct {
		name  string
		start []*Task
		want  []*Task
	}{
		{
			name: "A single simple task",
			start: []*Task{{
				position:    1,
				description: []string{"A", "simple", "task"},
			}},
			want: []*Task{{
				position:    1,
				description: []string{"A", "simple", "task"},
			}},
		},
		{
			name: "2 simple tasks",
			start: []*Task{
				{
					position:    1,
					description: []string{"A", "simple", "task"},
				},
				{
					position:    2,
					description: []string{"A", "simple", "task"},
				},
			},
			want: []*Task{
				{
					position:    1,
					description: []string{"A", "simple", "task"},
				},
				{
					position:    2,
					description: []string{"A", "simple", "task"},
				},
			},
		},
		{
			name: "2 tasks second has priority",
			start: []*Task{
				{
					position:    1,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "A",
					position:    2,
					description: []string{"A", "simple", "task"},
				},
			},
			want: []*Task{
				{
					Priority:    "A",
					position:    2,
					description: []string{"A", "simple", "task"},
				},
				{
					position:    1,
					description: []string{"A", "simple", "task"},
				},
			},
		},
		{
			name: "2 tasks first has priority",
			start: []*Task{
				{
					Priority:    "A",
					position:    1,
					description: []string{"A", "simple", "task"},
				},
				{
					position:    2,
					description: []string{"A", "simple", "task"},
				},
			},
			want: []*Task{
				{
					Priority:    "A",
					position:    1,
					description: []string{"A", "simple", "task"},
				},
				{
					position:    2,
					description: []string{"A", "simple", "task"},
				},
			},
		},
		{
			name: "A before B",
			start: []*Task{
				{
					Priority:    "B",
					position:    1,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "A",
					position:    2,
					description: []string{"A", "simple", "task"},
				},
			},
			want: []*Task{
				{
					Priority:    "A",
					position:    2,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "B",
					position:    1,
					description: []string{"A", "simple", "task"},
				},
			},
		},
		{
			name: "Duplicate A sorts on priority",
			start: []*Task{
				{
					Priority:    "A",
					position:    3,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "A",
					position:    1,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "A",
					position:    2,
					description: []string{"A", "simple", "task"},
				},
			},
			want: []*Task{
				{
					Priority:    "A",
					position:    1,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "A",
					position:    2,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "A",
					position:    3,
					description: []string{"A", "simple", "task"},
				},
			},
		},
		{
			name: "A mixed set",
			start: []*Task{
				{
					position:    1,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "B",
					position:    2,
					description: []string{"A", "simple", "task"},
				},
				{
					position:    3,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "A",
					position:    4,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "C",
					position:    5,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "B",
					position:    6,
					description: []string{"A", "simple", "task"},
				},
			},
			want: []*Task{
				{
					Priority:    "A",
					position:    4,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "B",
					position:    2,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "B",
					position:    6,
					description: []string{"A", "simple", "task"},
				},
				{
					Priority:    "C",
					position:    5,
					description: []string{"A", "simple", "task"},
				},
				{
					position:    1,
					description: []string{"A", "simple", "task"},
				},
				{
					position:    3,
					description: []string{"A", "simple", "task"},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := SortTasks(tt.start)
			assert.Check(t, cmp.DeepEqual(got, tt.want, gocmp.AllowUnexported(Task{})))
		})
	}
}
