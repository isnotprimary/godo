package godo

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"time"
)

// Config allows for interaction with the users TODOTXT files. Use NewConfig to create
// TODO Allow configuration of preserving priority on task completion
type Config struct {
	Env
}

// NewConfig creates a new Config with the provided env options
func NewConfig(env Env) *Config {
	return &Config{Env: env}
}

// Todotxt provides a file handle to the users todotxt file or an error. It creates the file in Env.TODODir if it does
// not exist. It is the callers' responsibility to close the file after use
func (c *Config) Todotxt() (*os.File, error) {
	return ensureFile(c.TODODir + "/todo.txt")
}

// Donetxt provides a file handle to the users done.txt or an error. It creates the file in Env.TODODir if it does not
// exist. It is the callers' responsibility to close the file after use
func (c *Config) Donetxt() (*os.File, error) {
	return ensureFile(c.TODODir + "/done.txt")
}

// AddTask appends a Task to the todotxt, creating the file in Env.TODODir if it does not exist. Ignores any position
// value that might exist in t
func (c *Config) AddTask(t *Task) (err error) {
	return c.addTasks([]*Task{t}, false, c.DateOnAdd)
}

func (c *Config) addTasks(tasks []*Task, archive, addDate bool) (err error) {
	var file *os.File
	if archive {
		file, err = c.Donetxt()
	} else {
		file, err = c.Todotxt()
	}
	if err != nil {
		return err
	}
	defer func() {
		cerr := file.Close()
		if err == nil {
			err = cerr
		}
	}()

	stat, err := file.Stat()
	if err != nil {
		return err
	}

	id := 1
	if stat.Size() != 0 {
		counted, err := countLines(file)
		if err != nil {
			return err
		}
		id = counted + 1

		_, err = file.Seek(-1, io.SeekCurrent)
		if err != nil {
			return err
		}

		// Check that we have a new line
		lastByte := make([]byte, 1)
		_, err = file.Read(lastByte)
		if err != nil {
			return err
		}

		// TODO detect and support windows line endings
		if string(lastByte) != "\n" {
			_, err = file.Write([]byte("\n"))
			if err != nil {
				return err
			}
		}
	}

	today := time.Now().Format(DateFormat)
	for _, t := range tasks {
		if t.CreationDate == "" && addDate {
			t.CreationDate = today
		}
		_, err = file.WriteString(fmt.Sprintf("%s\n", t.String()))
		if err != nil {
			return err
		}

		t.position = id
		id++
	}
	return nil
}

// UpdateTask updates a task in position in the todotxt. Use UpdateTasks for efficient updates to multiple tasks. See
// UpdateTasks for more details
func (c *Config) UpdateTask(t *Task) (err error) {
	return c.UpdateTasks([]*Task{t})
}

// UpdateTasks updates each task in the provided array in place in the todotxt
// This function is atomic. It steams tasks out to a temporary file and moves replaces the todotxt at the end.
func (c *Config) UpdateTasks(tasks []*Task) (err error) {
	todotxt, tmp, err := c.updateTasksToTemp(tasks)
	if err != nil {
		return err
	}

	err = os.Rename(tmp.Name(), todotxt.Name())
	if err != nil {
		return err
	}

	return nil
}

func (c *Config) updateTasksToTemp(tasks []*Task) (todotxt, tmp *os.File, err error) {
	todotxt, err = c.Todotxt()
	if err != nil {
		return nil, tmp, err
	}
	defer todotxt.Close()

	tmp, err = os.CreateTemp("", "todo.txt")
	if err != nil {
		return nil, nil, err
	}
	defer tmp.Close()

	taskByPos := make(map[int]*Task, len(tasks))
	for _, t := range tasks {
		taskByPos[t.Position()] = t
	}

	fileScanner := bufio.NewScanner(todotxt)
	count := 0
	for fileScanner.Scan() {
		count++
		var out []byte
		if t, ok := taskByPos[count]; ok {
			out = []byte(t.String())
			delete(taskByPos, count)
		} else {
			out = fileScanner.Bytes()
		}
		out = append(out, []byte("\n")...)
		_, err = tmp.Write(out)
		if err != nil {
			return nil, nil, err
		}
	}
	if err = fileScanner.Err(); err != nil {
		return nil, nil, err
	}

	if len(taskByPos) != 0 {
		missing := make([]int, 0, len(taskByPos))
		for pos := range taskByPos {
			missing = append(missing, pos)
		}
		sort.Ints(missing)
		return nil, nil, fmt.Errorf("task(s) %v not found", missing)
	}
	return todotxt, tmp, nil
}

// Tasks returns all tasks from the todotxt
func (c *Config) Tasks() (tasks []*Task, err error) {
	f, err := c.Todotxt()
	if err != nil {
		return nil, err
	}
	defer func() {
		cerr := f.Close()
		if err == nil {
			err = cerr
		}
	}()
	return c.tasks(f)
}

// DoneTasks returns all tasks from the donetxt
func (c *Config) DoneTasks() (tasks []*Task, err error) {
	f, err := c.Donetxt()
	if err != nil {
		return nil, err
	}
	defer func() {
		cerr := f.Close()
		if err == nil {
			err = cerr
		}
	}()
	return c.tasks(f)
}

func (c *Config) tasks(file *os.File) (tasks []*Task, err error) {
	i := 1
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		raw := scanner.Text()
		if raw != "" {
			t, err := Parse(raw)
			if err != nil {
				// Currently parse isn't very strict and therefore never errors.
				// Consider returning an error if this changes
				fmt.Printf("Warn: Invalid task: %s\n", scanner.Text())
			}
			t.position = i
			tasks = append(tasks, t)
		}
		i++
	}
	if err = scanner.Err(); err != nil {
		return nil, err
	}

	return tasks, nil
}

func ensureFile(filename string) (*os.File, error) {
	return os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0600)
}

func countLines(r io.Reader) (int, error) {
	fileScanner := bufio.NewScanner(r)
	count := 0
	for fileScanner.Scan() {
		count++
	}
	if err := fileScanner.Err(); err != nil {
		return 0, err
	}

	return count, nil
}

// DeleteTasks removes the given tasks from either the todotxt or, if archiveFile is true, from the donetxt
func (c *Config) DeleteTasks(tasks []*Task, archiveFile bool) (err error) {
	ids := make([]int, len(tasks))
	for i, task := range tasks {
		ids[i] = task.Position()
	}
	file, tmp, err := c.deleteTasks(ids, archiveFile)
	if err != nil {
		return err
	}

	err = os.Rename(tmp.Name(), file.Name())
	if err != nil {
		return err
	}

	return nil
}

// Delete remove tasks by position from either the todotxt or, if archiveFile is true, from the donetxt
func (c *Config) Delete(ids []int, archiveFile bool) (err error) {
	file, tmp, err := c.deleteTasks(ids, archiveFile)
	if err != nil {
		return err
	}

	err = os.Rename(tmp.Name(), file.Name())
	if err != nil {
		return err
	}

	return nil
}

func (c *Config) deleteTasks(ids []int, archiveFile bool) (file, tmp *os.File, err error) {
	if archiveFile {
		file, err = c.Donetxt()
	} else {
		file, err = c.Todotxt()
	}
	if err != nil {
		return nil, tmp, err
	}
	defer file.Close()

	tmp, err = os.CreateTemp("", "godo.txt")
	if err != nil {
		return nil, nil, err
	}
	defer tmp.Close()

	idMap := make(map[int]bool, len(ids))
	for _, id := range ids {
		idMap[id] = false
	}

	fileScanner := bufio.NewScanner(file)
	count := 0
	for fileScanner.Scan() {
		count++
		var out []byte
		if _, ok := idMap[count]; ok {
			delete(idMap, count)
		} else {
			out = fileScanner.Bytes()
			out = append(out, []byte("\n")...)
		}
		if len(out) > 0 {
			_, err = tmp.Write(out)
		}
		if err != nil {
			return nil, nil, err
		}
	}
	if err = fileScanner.Err(); err != nil {
		return nil, nil, err
	}

	if len(idMap) != 0 {
		missing := make([]int, 0, len(idMap))
		for pos := range idMap {
			missing = append(missing, pos)
		}
		sort.Ints(missing)
		return nil, nil, fmt.Errorf("task(s) %v not found", missing)
	}
	return file, tmp, nil
}

func (c *Config) DoTask(task *Task, archive bool) error {
	return c.doTasks([]*Task{task}, archive)
}

func (c *Config) DoTasks(tasks []*Task, archive bool) error {
	return c.doTasks(tasks, archive)
}
func (c *Config) doTasks(tasks []*Task, archive bool) error {
	if archive {
		toDelete := make([]int, len(tasks))
		for i, t := range tasks {
			t.MarkDone()
			toDelete[i] = t.Position()
		}

		todotxt, tmp, err := c.deleteTasks(toDelete, false)
		if err != nil {
			return err
		}

		err = c.addTasks(tasks, true, false)
		if err != nil {
			return err
		}

		err = os.Rename(tmp.Name(), todotxt.Name())
		if err != nil {
			return err
		}
	} else {
		for _, t := range tasks {
			t.MarkDone()
		}
		return c.UpdateTasks(tasks)
	}

	return nil
}
